module.exports = {
    parser: 'babel-eslint',
    extends: [
        'eslint:recommended',
        'plugin:react/recommended',
        'plugin:jsx-a11y/recommended',
        'plugin:flowtype/recommended',
    ],
    plugins: ['flowtype', 'react', 'jsx-a11y'],
    env: {
        browser: true,
        node: true,
    }
};