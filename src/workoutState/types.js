// @flow

export type LiveWorkoutData = {|
    durationCountdown: number, // seconds
    duration: number; // seconds
    grade: number; // %
    distance: number; // km
    pace: number; // seconds / km
    calories: number;
    speed: number; // km / hour
    heartRate: number;
|};

export type EntryWorkoutData = $Shape<LiveWorkoutData & {
    caloriesPerSecond: number;
    stepTime: number;
}>;

export type LiveWorkoutFormattedData = {|
    durationCountdown: string;
    duration: string;
    grade: string;
    distance: string;
    pace: string;
    calories: string;
    speed: string;
    heartRate: string;
|};

export type UnitSystem = 'METRIC' | 'IMPERIAL';
