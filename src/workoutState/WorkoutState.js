// @flow
import type {LiveWorkoutData, EntryWorkoutData} from './types';
import {defaultEntryData, defaultInitialLiveData, STEP_EVENT} from './config';

export class WorkoutState extends EventTarget {
    entryData: EntryWorkoutData;
    liveData: LiveWorkoutData;
    timeoutId: IntervalID;

    constructor(entryData: EntryWorkoutData = defaultEntryData) {
        super();

        this.entryData = entryData;
        this.liveData = defaultInitialLiveData;

        Object.keys(this.entryData)
            .filter(key => this.liveData.hasOwnProperty(key))
            .forEach(key => this.liveData[key] = this.entryData[key]);
    }

    emitStepEvent(detail: LiveWorkoutData) {
        this.dispatchEvent(new CustomEvent(STEP_EVENT, {detail}));
    }

    start() {
        this.timeoutId = setInterval(() => {
            const nextLiveData = this.calculateStep(this.liveData);
            this.liveData = nextLiveData;

            this.emitStepEvent(nextLiveData);

            if (nextLiveData.durationCountdown <= 0) {
                this.stop();
            }

        }, this.entryData.stepTime * 1000);
    }

    stop() {
        clearInterval(this.timeoutId);
    }

    reset() {
        this.liveData = defaultInitialLiveData;
        this.emitStepEvent(defaultInitialLiveData);
    }

    calculateStep(prevLiveData: LiveWorkoutData): LiveWorkoutData {
        const duration = prevLiveData.duration + this.entryData.stepTime;
        const durationCountdown = prevLiveData.durationCountdown - this.entryData.stepTime;
        const grade = this.liveData.grade;
        const calories = this.entryData.caloriesPerSecond * duration;
        const speed = this.liveData.speed;
        const distance = prevLiveData.distance + speed * this.entryData.stepTime / 60 / 60;
        const pace = 60 / speed * 60;
        const heartRate = this.liveData.heartRate;

        return {duration, durationCountdown, grade, distance, pace, calories, speed, heartRate};
    }
}
