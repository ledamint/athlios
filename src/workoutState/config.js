// @flow
import type {LiveWorkoutData, EntryWorkoutData} from './types';

export const defaultInitialLiveData: LiveWorkoutData = {
    duration: 0,
    durationCountdown: 0,
    grade: 0,
    distance: 0,
    pace: 0,
    calories: 0,
    speed: 0,
    heartRate: 0,
};

export const defaultEntryData: EntryWorkoutData = {
    stepTime: 0.5,
    durationCountdown: 1800,
    caloriesPerSecond: 0.1875,
    speed: 9,
    heartRate: 127,
    grade: 3,
};

export const STEP_EVENT = 'STEP_EVENT';

export const METRIC_SYSTEM = 'METRIC';
export const IMPERIAL_SYSTEM = 'IMPERIAL';
export const MILES_TO_KM_COEFFICIENT = 1.60934;