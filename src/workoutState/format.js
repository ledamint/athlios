// @flow
import {METRIC_SYSTEM, IMPERIAL_SYSTEM, MILES_TO_KM_COEFFICIENT} from './config';
import type {LiveWorkoutData, LiveWorkoutFormattedData, UnitSystem} from './types';

const getISOStringBySeconds = (seconds: number) => {
    const date = new Date(0);
    date.setSeconds(seconds);

    return date.toISOString();
};
const formatToHHMMSSBySeconds = (seconds: number) => getISOStringBySeconds(seconds).slice(11, 19);
const formatToMMSSBySeconds = (seconds: number) => getISOStringBySeconds(seconds).slice(14, 19);

const convertKilometersToMiles = (kilometers: number) => kilometers / MILES_TO_KM_COEFFICIENT;

const formatDistance = (distance: number, unitSystem: UnitSystem) => {
    let measurementUnit = 'km';

    if (unitSystem === IMPERIAL_SYSTEM) {
        measurementUnit = 'mi';
        distance = convertKilometersToMiles(distance);
    }

    return distance.toFixed(2) + measurementUnit;
};

const formatSpeed = (speed: number, unitSystem) => {
    let measurementUnit = 'km/h';

    if (unitSystem === IMPERIAL_SYSTEM) {
        measurementUnit = 'mi/h';
        speed = convertKilometersToMiles(speed);
    }

    return speed.toFixed(1) + measurementUnit;
};

const formatPace = (pace: number, unitSystem: UnitSystem) => {
    let measurementUnit = 'min/km';

    if (unitSystem === IMPERIAL_SYSTEM) {
        measurementUnit = 'min/mi';
        pace *= MILES_TO_KM_COEFFICIENT;
    }

    return formatToMMSSBySeconds(pace) + measurementUnit;
};

export const formatWorkoutData = (
    liveWorkoutData: LiveWorkoutData,
    unitSystem: UnitSystem = METRIC_SYSTEM
): LiveWorkoutFormattedData => {
    const duration = formatToHHMMSSBySeconds(Math.ceil(liveWorkoutData.duration));
    const durationCountdown = formatToHHMMSSBySeconds(Math.floor(liveWorkoutData.durationCountdown));
    const pace = formatPace(liveWorkoutData.pace, unitSystem);
    const distance = formatDistance(liveWorkoutData.distance, unitSystem);
    const grade = liveWorkoutData.grade.toFixed(1) + '%';
    const calories = String(Math.floor(liveWorkoutData.calories));
    const speed = formatSpeed(liveWorkoutData.speed, unitSystem);
    const heartRate = String(Math.floor(liveWorkoutData.heartRate));

    return {durationCountdown, duration, grade, distance, pace, calories, speed, heartRate};
};
