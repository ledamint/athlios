// @flow
import * as React from 'react';
import {connect} from 'react-redux'

import {Dashboard} from './Dashboard';
import {Video} from './Video';
import {EntertainmentSelect} from './EntertainmentSelect';
import {WorkoutState, STEP_EVENT, formatWorkoutData} from '../workoutState';
import type {LiveWorkoutData, UnitSystem} from '../workoutState/types';
import {videos, NONE_VIDEO_INDEX} from '../videos';
import {setActiveTab, setVideoIndex, setUnitSystem, setIsVideoPlaying, withSync} from '../store/actions';
import {getActiveTab, getVideoIndex, getUnitSystem, getIsVideoPlaying} from '../store/selectors';
import type {ActiveTab, GlobalState} from '../store/types';

type Props = {
    activeTab: ActiveTab;
    setActiveTab: (ActiveTab) => void;
    videoIndex: number;
    setVideoIndex: (number) => void;
    unitSystem: UnitSystem;
    setUnitSystem: (UnitSystem) => void;
    isVideoPlaying: boolean;
    setIsVideoPlaying: (boolean) => void;
};
type State = {
    liveWorkoutData: LiveWorkoutData;
};

const DASHBOARD_TAB: ActiveTab = 0;
const ENTERTAINMENT_SELECT_TAB: ActiveTab = 1;
const ENTERTAINMENT_TAB: ActiveTab = 2;

class App extends React.PureComponent<Props, State> {
    workoutState: WorkoutState = new WorkoutState();
    state = {
        liveWorkoutData: this.workoutState.liveData,
    };

    componentDidMount() {
        this.workoutState.addEventListener(STEP_EVENT, this.handleStep);
        this.workoutState.start();
    }

    componentWillUnmount() {
        this.workoutState.removeEventListener(STEP_EVENT, this.handleStep);
        this.workoutState.stop();
    }

    handleStep = (e: any) => {
        const liveWorkoutData: LiveWorkoutData = e.detail;

        this.setState({liveWorkoutData});
    }

    handleChangeUnitSystem = (unitSystem: UnitSystem) => {
        this.props.setUnitSystem(unitSystem);
    }

    handleSelectVideo = () => {
        this.props.setIsVideoPlaying(true);
    }

    handleChangeVideo = (videoIndex: number) => {
        this.props.setIsVideoPlaying(false);
        this.props.setVideoIndex(videoIndex)
    }

    handleClickVideo = () => {
        this.props.setActiveTab(ENTERTAINMENT_TAB);
    }

    renderTabContent(activeTab: ActiveTab) {
        switch (activeTab) {
            case DASHBOARD_TAB: {
                const {liveWorkoutData} = this.state;
                const {unitSystem} = this.props;

                return (
                    <Dashboard
                        unitSystem={unitSystem}
                        onChangeUnitSystem={this.handleChangeUnitSystem}
                        workoutData={formatWorkoutData(liveWorkoutData, unitSystem)}
                    />
                );
            }
            case ENTERTAINMENT_SELECT_TAB: {
                return (
                    <EntertainmentSelect
                        videos={videos}
                        videoIndex={this.props.videoIndex}
                        onSelectVideo={this.handleSelectVideo}
                        onChangeVideo={this.handleChangeVideo}
                    />
                );
            }
            default: {
                return null;
            }
        }
    }

    renderVideo() {
        const {activeTab, videoIndex, isVideoPlaying} = this.props;

        if (videoIndex === NONE_VIDEO_INDEX) {
            return null;
        }

        return (
            <Video
                isPlaying={isVideoPlaying}
                isHidden={activeTab === DASHBOARD_TAB}
                width={activeTab === ENTERTAINMENT_SELECT_TAB ? '30%' : '100%'}
                src={videos[videoIndex].src}
                onClick={this.handleClickVideo}
            />
        );
    }

    renderTabs(activeTab: number): React.Node[] {
        const tabs = [
            {name: 'Tab 1', tabIndex: DASHBOARD_TAB},
            {name: 'Tab 2', tabIndex: ENTERTAINMENT_SELECT_TAB},
            {name: 'Tab 3', tabIndex: ENTERTAINMENT_TAB},
        ];

        return tabs.map(({name, tabIndex}) => {
            return (
                <button
                    key={name}
                    onClick={() => this.props.setActiveTab(tabIndex)}
                    className={activeTab === tabIndex ? 'active' : ''}
                >
                    {name}
                </button>
            )
        });
    }

    render() {
        const {activeTab} = this.props;

        return (
            <div className="container">
                <div className="content">
                    {this.renderTabContent(activeTab)}
                    {this.renderVideo()}
                </div>
                <div className="tabs">
                    {this.renderTabs(activeTab)}
                </div>
            </div>
        );
    }
}

export const ConnectedApp = connect(
    (state: GlobalState): $Shape<Props> => ({
        activeTab: getActiveTab(state),
        videoIndex: getVideoIndex(state),
        unitSystem: getUnitSystem(state),
        isVideoPlaying: getIsVideoPlaying(state),
    }),
    {
        setActiveTab: withSync(setActiveTab),
        setVideoIndex: withSync(setVideoIndex),
        setUnitSystem: withSync(setUnitSystem),
        setIsVideoPlaying: withSync(setIsVideoPlaying),
    },
)(App);
