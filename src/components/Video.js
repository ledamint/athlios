// @flow
/* eslint-disable jsx-a11y/media-has-caption */
import React from 'react';

type Props = {
    isHidden: boolean;
    width: string;
    src: string;
    onClick: () => void;
    isPlaying: boolean;
};

export class Video extends React.Component<Props> {
    videoNode: HTMLVideoElement | null;

    componentDidMount() {
        this.toggleVideo();
    }

    componentDidUpdate() {
        this.toggleVideo();
    }

    toggleVideo() {
        if (this.videoNode) {
            if (this.props.isPlaying) {
                this.videoNode.play();
            } else {
                this.videoNode.pause();
            }
        }
    }

    render() {
        const {isHidden, width, src} = this.props;

        return (
            <video
                className={isHidden ? 'hidden' : ''}
                width={width}
                ref={node => this.videoNode = node}
                src={src}
                onClick={this.props.onClick}
            />
        );
    }
}
