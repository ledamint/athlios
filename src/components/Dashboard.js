// @flow
import * as React from 'react';
import type {LiveWorkoutFormattedData, UnitSystem} from '../workoutState/types';
import {IMPERIAL_SYSTEM, METRIC_SYSTEM} from '../workoutState';

type Props = {
    workoutData: LiveWorkoutFormattedData;
    onChangeUnitSystem: (UnitSystem) => void;
    unitSystem: UnitSystem;
};

export class Dashboard extends React.PureComponent<Props> {
    renderWorkoutData(workoutData: LiveWorkoutFormattedData): React.Node[] {
        return Object.entries(workoutData).map(([name, value]) => {
            return (
                <div key={name}>
                    <span>{name}</span> : <span>{String(value)}</span>
                </div>
            );
        });
    }

    render() {
        return (
            <div className="dashboard">
                {this.renderWorkoutData(this.props.workoutData)}
                <button
                    className={this.props.unitSystem === METRIC_SYSTEM ? 'active' : ''}
                    onClick={() => this.props.onChangeUnitSystem(METRIC_SYSTEM)}
                >
                    Metric
                </button>
                <button
                    className={this.props.unitSystem === IMPERIAL_SYSTEM ? 'active' : ''}
                    onClick={() => this.props.onChangeUnitSystem(IMPERIAL_SYSTEM)}
                >
                    Imperial
                </button>
            </div>
        );
    }
}
