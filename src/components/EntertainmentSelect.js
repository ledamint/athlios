/* eslint-disable jsx-a11y/no-onchange */
// @flow
import * as React from 'react';
import type {Video} from '../config';

type Props = {
    videos: Video[];
    videoIndex: number;
    onSelectVideo: () => void;
    onChangeVideo: (number) => void;
};

export class EntertainmentSelect extends React.PureComponent<Props> {
    handleChangeVideo = ({target: {value}}: SyntheticInputEvent<HTMLInputElement>) => {
        this.props.onChangeVideo(Number(value));
    }

    handleSelectVideo = () => {
        this.props.onSelectVideo();
    }

    renderOptions(videos: Video[]): React.Node[] {
        return videos.map(({name}, index) => {
            return <option key={name} value={index}>{name}</option>;
        });
    }

    render() {
        const {videoIndex, videos} = this.props;

        return (
            <div>
                <select
                    className="entertainment-select"
                    name="entertainment"
                    onChange={this.handleChangeVideo}
                    value={videoIndex}
                >
                    {this.renderOptions(videos)}
                </select>
                <button onClick={this.handleSelectVideo}>Select</button>
            </div>
        );
    }
}
