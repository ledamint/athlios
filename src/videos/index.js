// @flow
import killers from './killers.mp4';
import tameImpala from './tame_impala.mp4';

export type Video = {|
    name: string;
    src: string;
|}

export const NONE_VIDEO_INDEX = 0;

export const videos: Video[] = [
    {name: 'None', src: ''},
    {name: 'Killers', src: killers},
    {name: 'Tame Impala', src: tameImpala},
];