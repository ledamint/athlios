// @flow
import config from '../config';

type WsEventType = 'REDUX_ACTION';

export type WsEventData = {|
    type: WsEventType;
    payload?: any;
|}

export const wsEventsTypes = {
    REDUX_ACTION: 'REDUX_ACTION',
};

export const ws: WebSocket = new WebSocket(`ws://localhost:${config.ws.port}`);

export const wsSend = (data: WsEventData) => {
    ws.send(JSON.stringify(data));
};

export const getWsEventData = (event: MessageEvent): WsEventData => {
    if (typeof event.data !== 'string') {
        throw new Error('Provide data as a string');
    }

    return JSON.parse(event.data);
};
