// @flow
import {SET_ACTIVE_TAB, SET_VIDEO_INDEX, SET_UNIT_SYSTEM, SET_IS_VIDEO_PLAYING} from './actionTypes';
import type {UnitSystem} from '../workoutState/types';
import type {ActiveTab} from './types';
import {wsEventsTypes, wsSend} from './ws';

export const setActiveTab = (activeTab: ActiveTab) => {
    return {
        type: SET_ACTIVE_TAB,
        payload: activeTab,
    }
};

export const setVideoIndex = (videoIndex: number) => {
    return {
        type: SET_VIDEO_INDEX,
        payload: videoIndex,
    }
};

export const setUnitSystem = (unitSystem: UnitSystem) => {
    return {
        type: SET_UNIT_SYSTEM,
        payload: unitSystem,
    }
};

export const setIsVideoPlaying = (isVideoPlaying: boolean) => {
    return {
        type: SET_IS_VIDEO_PLAYING,
        payload: isVideoPlaying,
    }
};

export const withSync = (actionCreator: Function) => (payload: any) => (dispatch: Function) => {
    const action = actionCreator(payload);

    wsSend({type: wsEventsTypes.REDUX_ACTION, payload: action});
    dispatch(action);
};