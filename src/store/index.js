// @flow
import {createStore, applyMiddleware} from 'redux';
import {reducer} from './reducers';
import thunk from 'redux-thunk';
import {getWsEventData, ws, wsEventsTypes} from './ws';

export const store = createStore(reducer, applyMiddleware(thunk));

ws.addEventListener('message', (event: MessageEvent) => {
    const data = getWsEventData(event);

    if (data.type === wsEventsTypes.REDUX_ACTION) {
        store.dispatch(data.payload);
    }
});