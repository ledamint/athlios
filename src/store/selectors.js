// @flow
import type {GlobalState} from './types';

export const getActiveTab = (state: GlobalState) => {
    return state.activeTab;
};

export const getVideoIndex = (state: GlobalState) => {
    return state.videoIndex;
};

export const getUnitSystem = (state: GlobalState) => {
    return state.unitSystem;
};

export const getIsVideoPlaying = (state: GlobalState) => {
    return state.isVideoPlaying;
};