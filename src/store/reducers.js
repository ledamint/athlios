// @flow
import {SET_ACTIVE_TAB, SET_IS_VIDEO_PLAYING, SET_UNIT_SYSTEM, SET_VIDEO_INDEX} from './actionTypes';
import type {GlobalState, Action} from './types';
import {METRIC_SYSTEM} from '../workoutState';

const initialState = {
    activeTab: 0,
    videoIndex: 0,
    unitSystem: METRIC_SYSTEM,
    isVideoPlaying: false,
};

export const reducer = (state: GlobalState = initialState, action: Action): GlobalState => {
    switch (action.type) {
        case SET_ACTIVE_TAB: {
            return {...state, activeTab: action.payload}
        }
        case SET_VIDEO_INDEX: {
            return {...state, videoIndex: action.payload}
        }
        case SET_UNIT_SYSTEM: {
            return {...state, unitSystem: action.payload}
        }
        case SET_IS_VIDEO_PLAYING: {
            return {...state, isVideoPlaying: action.payload}
        }
        default: {
            return state;
        }
    }
};