// @flow

import type {UnitSystem} from '../workoutState/types';

export type ActiveTab = 0 | 1 | 2;

export type Action = {
    type: string;
    payload: any;
}

export type GlobalState = {
    activeTab: ActiveTab;
    videoIndex: number;
    unitSystem: UnitSystem;
    isVideoPlaying: boolean;
};
