const Koa = require('koa');
const serve = require('koa-static');
const WebSocket = require('ws');
const config = require('./config');

const wss = new WebSocket.Server({ port: config.ws.port });
wss.on('connection', (ws) => {
    ws.on('message', (message) => {
        wss.clients.forEach((client) => {
            if (client !== ws) {
                client.send(message);
            }
        })
    });
});

const app = new Koa();
app.use(serve('./build'));

config.apps.forEach(({port}) => {
    app.listen(port);
    /* eslint-disable-next-line no-console */
    console.log(`App has started at port ${port}`);
});
